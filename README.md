# I Love Zappos for Android Development Internship
----
I Love Zappos is a basic Android application that takes user input as a search query and uses the Zappos API and returns and displays the first product returned. 

![I Love Zappos](/i_love_zappos_start_img.png)

----
## Dev on
- Google Pixel XL on 7.1.1
- Google Nexus 5 on 6.0.1
