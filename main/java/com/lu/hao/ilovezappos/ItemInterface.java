package com.lu.hao.ilovezappos;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

// ItemInterface is used to get the correct path from the base url
public interface ItemInterface {
    // Takes a term, a String, which is the item the use searched for
    // Take a key, a String, the API key to access the API data
    @GET("Search?")
    Call<ItemResponse> getSearchResult(@Query("term") String term, @Query("key") String apiKey);
}
