package com.lu.hao.ilovezappos;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

public class DataBindingAdapters {

    // Data binding adapter binds the attribute to the specific method: loadImage below
    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if (!url.equals("")) {
            Picasso.with(imageView.getContext()).load(url).into(imageView);
//            Picasso.with(imageView.getContext()).load(url).resize(1000, 1000).into(imageView);

        }
    }
}
