package com.lu.hao.ilovezappos;

import android.app.SearchManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.lu.hao.ilovezappos.databinding.ActivitySearchableBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchableActivity extends AppCompatActivity {

    public static final String BASE_URL  = "https://api.zappos.com/";
    private static Retrofit retrofit;
    private ProgressBar progressBar;
    private String apiKey = "b743e26728e16b81da139182bb2094357c31d331";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
             new SearchZapposTask().execute(query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                // Goes back to the parent activity and starts it onCreate()
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_cart:
                Toast.makeText(this, "Go to cart", Toast.LENGTH_SHORT).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);
    }

    // AsyncTask allows task to be performed in the background and no in the UIThread, which can
    // cause the whole app to crash if the task takes too much time. The asynchronous task is good
    // for short task and the results are publish on the UIThread.
    private class SearchZapposTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(String... strings) {
            // Create retrofit
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ItemInterface itemInterface = retrofit.create(ItemInterface.class);
            Call<ItemResponse> call = itemInterface.getSearchResult(strings[0], apiKey);
            call.enqueue(new Callback<ItemResponse>() {
                @Override
                public void onResponse(Call<ItemResponse> call, retrofit2.Response<ItemResponse> response) {
                    if (response.isSuccessful()) {
                        ItemResponse itemResponse = response.body();
                        ActivitySearchableBinding binding = DataBindingUtil.setContentView(
                                SearchableActivity.this, R.layout.activity_searchable);
                        Item item = new Item(itemResponse.getItems().get(0).getProductName());
                        item.setThumbnailImageUrl(itemResponse.getItems().get(0).getThumbnailImageUrl());
                        item.setBrandName(itemResponse.getItems().get(0).getBrandName());
                        item.setPrice(itemResponse.getItems().get(0).getPrice());
                        binding.setItem(item);
                        SearchableActivity.MyHandlers handlers = new SearchableActivity.MyHandlers();
                        binding.setHandlers(handlers);
                    }
                }
                @Override
                public void onFailure(Call<ItemResponse> call, Throwable t) {

                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
        }
}

    // Handler for event handling using method references. Animates the fab when it is clicked.
    // Inner class to get Context for animation.
    public class MyHandlers {
        public void onClickFAB(View view) {
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            Animation fabPulse = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_pulse);
            Animation fabCollapse = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_collapse);
            // Start animation
            fab.startAnimation(fabCollapse);
            fab.startAnimation(fabPulse);
            fab.setImageResource(R.drawable.ic_check);

        }
    }
}
